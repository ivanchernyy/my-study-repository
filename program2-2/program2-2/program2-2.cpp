#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

void main()
{
	printf("char min=%d, max=%d, uchar min=0, max=%d\n", CHAR_MIN, CHAR_MAX, UCHAR_MAX);
	printf("short min=%d, max=%d, umax=%d\n", SHRT_MIN, SHRT_MAX, USHRT_MAX);
	printf("int min=%d, max=%d, umax=%d\n", INT_MIN, INT_MAX, UINT_MAX);
	printf("long min=%d, max=%d, umax=%d\n", LONG_MIN, LONG_MAX, ULONG_MAX);
	printf("long long min=%d, max=%d, umax=%d\n", LLONG_MIN, LLONG_MAX, ULLONG_MAX);
	printf("float min positiv value=%f, max=%f\n", FLT_MIN, FLT_MAX);
	printf("double min positiv value=%f, max=%f\n", DBL_MIN, DBL_MAX);
	printf("long double min positiv value=%f, max=%f\n", LDBL_MIN, LDBL_MAX);
	system("PAUSE");
}