#include <stdio.h>
#include <stdlib.h>

int main()
{
	int c, nt, ns, nl, isPrevSpace, isSymbol;

	printf("Enter text:\n");

	nt = 0;
	ns = 0;
	nl = 0;
	while ((c=getchar()) != EOF)
	{
		if (c == '\t')
			++nt;
		if (c == ' ')
			++ns;
		if (c == '\n')
			++nl;
	}

	printf("\nNumber of tabs in text: %d\nSpaces: %d\nEnd of lines: %d\n",nt,ns,nl);
	system("PAUSE");

	printf("\nEnter text to remove all multiple spaces:\n");
	isPrevSpace = 0;
	while ((c=getchar()) != EOF)
	{
		if (c == ' ')
		{
			if (isPrevSpace == 0)
			{
				putchar(c);
				isPrevSpace = 1;
			}
		}
		else
		{
			putchar(c);
			isPrevSpace = 0;
		}
	}
	system("PAUSE");

	printf("\nProgram will replace all tabs, slashes and backspaces to their sybols\n");
	while ((c=getchar()) != EOF)
	{
		isSymbol = 0;
		if (c == '\t')
		{
			printf("\\t");
			isSymbol = 1;
		}
		if (c == '\b')
		{
			printf("\\b");
			isSymbol = 1;
		}
		if (c == '\\')
		{
			printf("\\\\");
			isSymbol = 1;
		}
		if (isSymbol == 0)
			putchar(c);
	}

	system("PAUSE");
}