#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

void main()
{
	int c, number, freeplace, i, j;
	int freq[SIZE], type[SIZE];

	for (i=0; i<SIZE; ++i)
	{
		freq[i] = 0;
		type[i] = -1;
	}

	freeplace = 0;
	while((c = getchar()) != EOF)
	{
		//���������, ��� �� ����� ������ ������ �����
		number = -1;
		for (i=0; i<SIZE; ++i)
		{
			if (type[i] == c)
				number = i;
		}

		//���� ������ ������� � ������� ���,
		if (number == -1)
		{
			//�� ��� ���� ����� ��� ����� ��� �������
			if (freeplace < SIZE)
			{
				type[freeplace] = c; //������ ����� ��� �������
				++freq[freeplace];   //��������� ������� ������� �������
				++freeplace;         //������� ������� ������� ����� ������
			}
		}
		//����� ��������� ������� �������
		else
		{
			++freq[number];
		}
	}

	printf("\nHere frequency of symbols:\n");
	for (i=0; i<freeplace; ++i)
	{
		if (type[i] == '\n')
			printf("\\n");
		else
			putchar(type[i]);
		printf(" = %d\n", freq[i]);
	}

	printf("\nHistogram:\n\n");
	for (i=0; i<freeplace; ++i)
	{
		for (j=0; j<freq[i]; ++j)
		{
			if (type[i] == '\n')
				printf("\\n");
			else
				putchar(type[i]);
		}
		printf("\n");
	}

	printf("\n");
	system("PAUSE");
}