#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000

int getline(char line[], int maxline);

void copy(char to[], char from[]);

/* ������ ����� ������� ������ */
int main()
{
	int len; /* ����� ������� ������ */
	int max; /* ����� ������������ �� ������������� ����� */
	char line[MAXLINE]; /* ������� ������ */
	char longest[MAXLINE]; /* ����� ������� ������ */

	max = 0;
	while ((len = getline(line, MAXLINE)) > 0)
		if (len > max)
		{
			max = len;
			copy(longest, line);
		}

	if (max > 0) /* ���� �� ���� ���� ������? */
		printf("%s", longest);
	
	system("PAUSE");
	return 0;
}

/* getline: ������ ������ � s, ���������� ����� */
int getline(char s[], int lim)
{
	int c, i, flag;
	
	//for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i)
	//	s[i] = c;
	flag = 0;
	i = 0;
	while (flag == 0)
	{
		flag = 1;

		if (i < lim-1)
			if ((c = getchar()) != EOF)
				if (c != '\n')
				{
					s[i] = c;
					++i;
					flag = 0;
				}
	}
	
	if (c == '\n')
	{
		s[i] = c;
		++i;
	}
	
	s[i] = '\0';
	
	return i;
}

/* copy: �������� �� 'from' � 'to'; to ���������� ������� */
void copy(char to[], char from[])
{
	int i;

	i = 0;
	while ((to[i] = from[i]) != '\0')
		++i;
}