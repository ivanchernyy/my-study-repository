#include <stdio.h>
#include <stdlib.h>

#define MAXLENGTH 7
#define IN 1
#define OUT 0

void main()
{
	int c, l, i, j, state;
	int count[MAXLENGTH];

	printf("Enter text:\n");

	//counting of words
	//�������� ������
	for (i = 0; i < MAXLENGTH; ++i)
		count[i] = 0;

	state = OUT;
	l = 0;
	while((c = getchar()) != EOF)
	{
		if (c == ' ' || c == '\t' || c == '\n' || c == ',' || c == '.' || c == ';' || c == '!' || c == '?' || c == '\"')
		{
			//when word's end's
			if (state == IN)
			{
				if (l <= MAXLENGTH)
					++count[l-1]; //����� ���� ����� L ���������� �� �������
				else
					++count[MAXLENGTH-1]; //������� ������� �����
				//� ������� ����� ����� ������������
				l = 0;
			}
			state = OUT;
		}
		else
		{
			state = IN;
			++l;
		}
	}

	//���� while �� ������� ��������� �����, ���� ����� ����� ����
	//���� ������ ����� �����. ��������� ������ ���������� ���
	if (l != 0)
	{
		if (l <= MAXLENGTH)
			++count[l-1]; //����� ���� ����� L ���������� �� �������
		else
			++count[MAXLENGTH-1]; //������� ������� �����
	}

	//����� ����������
	for (i=0; i<MAXLENGTH; ++i)
		printf("L=%d: %d\n",i+1,count[i]);

	//����� �����������
	printf("\nMax length of word is %d\nHistogram:\n", MAXLENGTH);
	for (i=0; i<MAXLENGTH; ++i)
	{
		printf("%d: ",i+1);
		for (j=0; j<count[i]; ++j)
			printf("-");
		printf("\n");
	}
	system("PAUSE");
}