#include <stdio.h>

int FahrToCelsius(int fahr)
{
	return (5.0/9.0)*(fahr-32.0);
}
int main()
{
	int fahr;

	for (fahr = 300; fahr >= 0; fahr = fahr - 20)
		printf("%3d\t%6.2f\n", fahr, FahrToCelsius(fahr));
	getchar();
	return 0;
}