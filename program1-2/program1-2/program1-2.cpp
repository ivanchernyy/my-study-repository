#include <stdio.h>

/* ����� ������� ���������� �� ���������� � �������
��� fahr = 0, 20, ..., 300
� ����� �������� �������� ��� ������� ���������
� ��������� ��������� � �������*/

int main()
{
	float fahr, celsius;
	int lower, upper, step;

	//��������� -> �������
	lower = 0;
	upper = 300;
	step = 20;

	printf("Farenheit -> Celsius\n\n");
	fahr = lower;
	while (fahr <= upper)
	{
		celsius = 5.0 * (fahr-32.0) / 9.0;
		printf("  %3.0f\t%6.1f \n", fahr, celsius);
		fahr = fahr + step;
	}

	/*����� ���� ����� ������ ��������, ���� ������� � ��� �� ����� ��������
	�������� ���:

	printf("\nCelsius -> Farenheit\n\n");
	while (fahr >= lower)
	{
		celsius = 5.0 * (fahr-32.0) / 9.0;
		printf("  %3.0f\t%6.1f \n", celsius, fahr);
		fahr = fahr - step;
	}

	� �����, �� �� �����, �� � �������� �������. ������������ �������� ����,
	�� ������ � ������ ������������ ������� ������.
	*/

	//������� -> ���������
	lower = -100;
	upper = 200;

	printf("\nCelsius -> Farenheit\n\n");
	celsius = lower;
	while (celsius <= upper)
	{
		fahr = 32.0 + 9.0 * celsius / 5.0;
		printf("  %3.0f\t%4.0f \n", celsius, fahr);
		celsius = celsius + step;
	}
	getchar();
}