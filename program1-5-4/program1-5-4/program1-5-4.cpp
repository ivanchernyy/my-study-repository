#include <stdio.h>
#include <stdlib.h>

#define IN 1
#define OUT 0

void main()
{
	int c, state;
	printf("Every word on new line. Enter text:\n");

	state = IN;
	while((c = getchar()) != EOF)
	{
		if (c == ' ' || c == '\t')
			state = OUT;
		else
		{
			if (state == OUT)
				printf("\n");
			state = IN;
			putchar(c);
		}
	}
	system("PAUSE");
}