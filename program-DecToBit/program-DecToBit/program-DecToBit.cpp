#include <stdio.h>
#include <stdlib.h>

#define SIZE 32*8 //size of bit number

int sqrt(int a);

void main()
{
	int a, i;
	char s[SIZE];

	printf("Enter value: ");
	a = 77;
	printf("a = %d\n",a);

	i = sqrt(a);
	s[i+1] = '\0';
	while (i >= 0)
	{
		if ((a % 2) == 0)
			s[i] = '0';
		else
			s[i] = '1';
		a = a / 2;
		--i;
	}

	printf("Value is %s\n",s);
	system("PAUSE");
}

int sqrt(int a)
{
	int i = 0;

	while (a >= 2)
	{
		a = a >> 1;
		++i;
	}

	return i;
}