#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("Program illustrate working with input flow\n");

	char c;
	int d;

	c = getchar();
	while (c != '\n')
	{
		putchar(c);
		c = getchar();
	}

	printf("\n");

	d = (getchar() != '\n');
	printf("%d\n",d);

	printf("Symbol of EOF is %d\n", EOF);

	system("PAUSE");
}