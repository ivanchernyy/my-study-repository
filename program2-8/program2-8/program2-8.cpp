#include <stdio.h>
#include <stdlib.h>

#define SIZE 100  //����������� ���������� ����� ������

void squeeze(char a[], char b[]);

void main()
{
	char a[SIZE], b[SIZE];
	char c;
	int i;

	//�� ������ � ������� �������, ������������ � ������ �
	printf("Chars from A will be cut from B.\nEnter A: ");
	c = getchar();
	for (i=0; c != '\n'; ++i)
	{
		a[i] = c;
		c = getchar();
	}	
	a[i] = '\0';
	printf("A is \"%s\"\n",a);

	printf("Enter B: ");
	c = getchar();
	for (i=0; c != '\n'; ++i)
	{
		b[i] = c;
		c = getchar();
	}
	b[i] = '\0';
	printf("B is \"%s\"\n",b);

	squeeze(a,b);

	printf("Result: %s\n",b);
	system("PAUSE");
}

void squeeze(char a[], char b[])
{
	int i, j, k, match;
	
	//��-�������� ����� ��� � ����� �������� �������� ��
	//������������ ����� �����, ����� � ������ ��� �������
	//��������� \0? � ����� ������� ��������
	k = 0;
	for (i = 0; b[i] != '\0'; ++i)
	{
		match = 0;
		for (j = 0; a[j] != '\0'; ++j)
			if (b[i] == a[j])
				match = 1;

		if (match == 0)
		{
			b[k] = b[i];
			++k;
		}
	}

	b[k] = '\0';
}